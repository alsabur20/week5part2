# Basic libraries to import for completing the whole work.
import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv


class Mainwindow(QMainWindow):
    def __init__(self):
        super(Mainwindow, self).__init__()
        loadUi("main.ui", self)  # Here we imported the QT Designer file which we made as Python GUI FIle.

        self.setWindowTitle("Python ")
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.submitBtn.clicked.connect(self.Submit)
        
    def ResetValues(self):
        self.District.setCurrentIndex(0)
        self.Tehsil.setCurrentIndex(0)
        self.StampPaperType.setCurrentIndex(0)
        self.DeedName.setCurrentIndex(0)
        self.agentName.setText("")
        self.agentContact.setText("")
        self.agentCNIC.setText("")
        self.agentEmail.setText("")
        
    def Submit(self):
        district=self.District.currentText()
        tehsil=self.Tehsil.currentText()
        stampPaper=self.StampPaperType.currentText()
        deedName=self.DeedName.currentText()
        agentName=self.agentName.toPlainText()
        agentCNIC=self.agentCNIC.toPlainText()
        agentContact=self.agentContact.toPlainText()
        agentEmail=self.agentEmail.toPlainText()
        
        data=[]
        tempdata=[district,tehsil,stampPaper,deedName,agentName,agentContact,agentCNIC,agentEmail]
        data.append(tempdata)
        with open('challan.csv', "a", encoding="utf-8", newline="") as fileInput:
                writer = csv.writer(fileInput)
                writer.writerows(data)
        self.ResetValues()
        
# main

app = QApplication(sys.argv)
window = Mainwindow()
window.show()
sys.exit(app.exec_())
